<?php
include_once "components/Functions.php";

//check the session and the session variables and set them if they aren't set
if(session_status() !== PHP_SESSION_ACTIVE) session_start();
if(!isset($_SESSION["orders"])) {$_SESSION["orders"] = [];}
if(!isset($_SESSION["price_order"])) {$_SESSION["price_order"] = 0;}
if(!isset($_SESSION["logged_in"])) {$_SESSION["logged_in"] = 0;}

if(!isset($_SESSION["queried_pizzas"]) OR !isset($_SESSION["queried_extras"])) {

    // Create connection
    include_once ("config/db_config.php");
    $conn = new mysqli(DBHOST, DBUSER, DBPWD, DBNAME);
    $conn->set_charset("utf8");

    // Check connection
    if ($conn->connect_error) {
        die("Es ist ein Problem aufgetreten, bitte versuchen Sie es später erneut");
    }
    echo "<script> console.log('Connected successfully') </script>";
    $extras = $conn->query("SELECT * FROM Extras;");
    $_SESSION["queried_extras"] = $extras->fetch_all(MYSQLI_ASSOC);
    $pizzas = $conn->query("SELECT p.name, max(p.description) AS description, GROUP_CONCAT(e.name SEPARATOR ', ') AS ingredients, GROUP_CONCAT(e.ID SEPARATOR ',') AS extras_ID,max(p.price) AS price,max(p.ID) AS ID  FROM Extras AS e JOIN Pizza_has_Extra AS h ON e.ID = h.Extras_ID  JOIN Pizzas AS p ON p.ID = h.Pizzas_ID GROUP BY p.name ORDER BY ID;");
    $_SESSION["queried_pizzas"] = $pizzas->fetch_all(MYSQLI_ASSOC);

    $conn->close();
}

if (!isset($_SESSION["discount_pizza"])) {
//algorithm to determine the daily discounted pizza, the permutation will start over every month,
// but it still fulfills its purpose in my opinion
    $day = date("d");
    $index = $day - 1;
    $pizza_count = count($_SESSION["queried_pizzas"]);
    $_SESSION["discount_pizza"] = null;

    //check if the day is bigger than the amount of pizzas
    if ($day > $pizza_count) {
        $iteration = ((int)($day / $pizza_count));
        if (($day % $pizza_count) === 0) {
            $iteration -= 1;
        }
        //restart the iteration of the pizzas
        //e.g. if there are 9 pizzas at day 10 and 19 and 28 it will star with the first pizza again
        $index = $day - 1 - ($pizza_count * $iteration);
    }
    $_SESSION["discount_pizza"] = &$_SESSION["queried_pizzas"][$index];
    $_SESSION["discount_pizza_old_price"] = $_SESSION["discount_pizza"]["price"];
    $_SESSION["discount_pizza"]["price"] = number_format($_SESSION["discount_pizza"]["price"] * 0.67, 2);
//
}
?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pizza Plaza</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="node_modules/bootstrap-icons/font/bootstrap-icons.css" >
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
    <a class="navbar-brand" href="?site=main">Pizza Plaza</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto">
            <li class="nav-item">
                <a class="nav-link <?php if($currentSite === 'main') echo 'active'; ?>" href="?site=main">Startseite</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($currentSite === 'order') echo 'active'; ?>" href="?site=order">Online-Bestellung</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($currentSite === 'about') echo 'active'; ?>" href="?site=about">&Uuml;ber uns</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($currentSite === 'contact') echo 'active'; ?>" href="?site=contact">Kontakt</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($currentSite === 'imprint') echo 'active'; ?>" href="?site=imprint">Impressum |</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($currentSite === 'backend') echo ' active'; ?>" href="?site=backend"><i class="bi bi-lock"></i> Mitarbeiter</a>
            </li>
            <li class="nav-item">
                <a  href="components/Login.php?logout=1" class="nav-link <?php if($_SESSION["logged_in"] == 0) echo ' disabled " tabindex="-1" aria-disabled="true"'; else echo '"'?> ><i class="bi bi-door-open"></i> Logout</a>
            </li>
        </ul>
        <ul class="navbar-nav fs-5 me-4 ms-auto">
            <li class="nav-item">
                <a class="nav-link <?php if($currentSite === 'checkout') echo 'active'; ?>" href="?site=checkout"><i class="bi bi-cart-fill"></i> Warenkorb: <?php echo number_format($_SESSION['price_order'],2) ?>€</a>
            </li>
        </ul>
    </div>
    </div>
</nav>

<!-- Very simple breadcrumb function, which won't work with deeper site structures then one subpage under the index, but it is enough for this project in my opinion   -->
<div class="breadcrumb">
    <?php function get_Name($site){
        switch ($site) {
            case "about": return "Über uns";
            case "contact": return "Kontakt";
            case "imprint": return "Impressum";
            case "order": return "Online-Bestellung";
            case "checkout" : return  "Warenkorb";
            case "information": return "Persönliche Daten";
            case "backend" : return "Mitarbeiteransicht";
            default:
                return "Startseite";
        }
    }?>
    <nav aria-label="breadcrumb">
        <div class="container-fluid">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#" class="text-dark text-decoration-none">Pizza Plaza</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo get_Name($currentSite) ?></li>
            </ol>
        </div>
    </nav>
</div>

<div class="container">

<?php
    if(isset($_SESSION["order_success"]) and $_SESSION["order_success"] === 1){
        echo '<div class="alert alert-success alert-dismissible fade show fs-4" role="alert">
              Vielen Dank! Ihre Bestellung wurde erfolgreich gespeichert.
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>';
        $_SESSION["order_success"] = 0;
    }
    if(isset($_SESSION["order_error"]) and $_SESSION["order_error"] === 1){
        echo '<div class="alert alert-danger alert-dismissible fade show fs-4" role="alert">
              Sie haben keine Waren für die Bestellung ausgewählt
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>';
        $_SESSION["order_error"] = 0;
    }
?>

    <?php include $currentSite . '.php'; ?>
</div>


<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="assets/js/main.js"></script>
<script src="node_modules/bootstrap-input-spinner/src/input-spinner.js"></script>
<script>$("input[type='number']").inputSpinner()</script>
</body>
</html>
