<?php

echo '  <h1 class="">Bestellung abschließen</h1>';
        foreach ($_SESSION["orders"] as $key => $order_item){
            echo '<div class="row bg-light border text-dark px-3 py-3 m-2 fs-5">
                      <div class="col-md-8">
                           <p class="fw-bold"> Nr. ' . $order_item["pizza"]["ID"] . ' Pizza ' . $order_item["pizza"]["name"] . ' <br><span class="font-italic"> ' . $order_item["pizza"]["description"] . ' </span></p>
                           <p> Zutaten: <span class="badge bg-warning m-1 fs-6 text-wrap">' . $order_item["pizza"]["ingredients"] . '</span></p>
                           <p> Ausgewählte Extras: ';
                            foreach ($order_item["extras"] as $extra) {
                                echo '<span class="badge bg-warning m-1 fs-6">' . $extra["name"] . " +" . $extra["price"] . '€</span>';

                            }
                            if(empty($order_item["extras"])){echo '<span class="badge bg-danger m-1 fs-6">Keine Extras gewählt</span>';}
                            echo '
                            </p>
                            <form action="components/Order_Item_Change.php" method="post" id="delete' . $key . '">
                                    <input type="hidden" name="delete_id" value="' . $key . '">
                            </form>
                       </div>
                   <div class="col-md-4 justify-content-end text-end">
                        <p class="">' . number_format(($order_item["price_item"]/$order_item["amount"]), 2) . ' € pro Pizza (inklusive Extras)</p>
                        <p class="fw-bold">Anzahl: ' . $order_item["amount"] . ' Stück</p>
                        <p class="fw-bold">Preis: ' . number_format($order_item["price_item"], 2) . ' €</p>
                        <div>
                            <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#ModalPizza' . $key . '"><i class="bi bi-pencil-fill"></i> Ändern</button>                              
                            <button type="submit" form="delete' . $key . '" class="btn btn-danger"><i class="bi bi-trash"></i> Löschen</button>
                        </div>
                   </div>
                   </div>                  
                  <!-- bootstrap-modal wich pops up when you click the order button and lets you select your wished extras -->    
                   <div class="modal fade" id="ModalPizza' . $key . '" tabindex="-1" aria-labelledby="ModalPizza' . $key . '" aria-hidden="true">
                       <div class="modal-dialog modal-dialog-centered">
                           <div class="modal-content">
                               <div class="modal-header">
                                   <h5 class="modal-title" id="ModalLabel">Extras auswählen</h5>
                               </div>
                               <form action="components/Order_Item_Change.php" method="post" id="extras' . $key . '">
                               <div class="modal-body d-flex justify-content-start flex-wrap">
                                   <input type="hidden" name="change_id" value="' . $key . '">';
                               foreach ($_SESSION["queried_extras"] as $extra) {
                                    if ($extra["isChoosable"]==true){
                                        echo '<div class="mt-1 ms-1 text-dark">';
                                        if(searchMultiArray($extra["ID"],$order_item["extras"])){
                                            echo  '<input type="checkbox" class="btn-check " name="extras[]" id="extra' . $key . $extra["ID"] . '" checked value="' . $extra["ID"] . '" autocomplete="off">
                                                   <label class="btn btn-outline-warning" for="extra' . $key . $extra["ID"] . '" ' . $extra["ID"] . '">' . $extra["name"] . " +" . $extra["price"] . ' €</label>';
                                        }
                                        else{
                                            echo  '<input type="checkbox" class="btn-check" name="extras[]" id="extra' . $key . $extra["ID"] . '" value="' . $extra["ID"] . '" autocomplete="off">
                                                   <label class="btn btn-outline-warning" for="extra' . $key . $extra["ID"] . '" ' . $extra["ID"] . '">' . $extra["name"] . " +" . $extra["price"] . ' €</label>';
                                        }
                                        echo '</div>';
                                    }
                               }
                        echo'     <div class="w-100 m-1">
                                    <label for="amount">Anzahl:</label>
                                    <p class=""> <input class="form-control-sm" name="amount" required type="number"  id="amount" value="' . $order_item["amount"] . '" min="1" max="30"> </p> 
                                  </div>   
                               </form>
                               </div>
                               <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Zurück</button>
                                    <button type="submit" form="extras' . $key . '" value="Submit" class="btn btn-warning"><i class="bi bi-pencil-fill"></i> Ändern</button>
                               </div>
                           </div>
                        </div>
                   </div>';
        
    }
    if ($_SESSION["orders"] === []){
        echo '<div class="w-100 fs-3 badge bg-danger p-2">Keine Waren ausgewählt</div>';
    }else {
        echo '<div class="row bg-secondary border justify-content-end align-content-center text-light px-3 py-3 m-2 fs-3">
                    <div class="col-12 col-md-3 align-self-center text-center fw-bold">Gesamtpreis: ' . number_format($_SESSION["price_order"], 2) . '€</div>
                    <a class="col-12 col-md-3 btn btn-warning fs-3" href="?site=information"><i class="bi bi-check2"></i> Weiter</a>
              </div>';
    }