<?php

//if logged in, query the DB for all made orders with their details (orderItems etc.) and customers and display them
if ($_SESSION['logged_in'] == 1) {

    // Create connection
    include_once("config/db_config.php");
    $conn = new mysqli(DBHOST, DBUSER, DBPWD, DBNAME);
    $conn->set_charset("utf8");

    // Check connection
    if ($conn->connect_error) {
        die("Es ist ein Problem aufgetreten, bitte versuchen Sie es später erneut");
    }
    echo "<script> console.log('Connected successfully') </script>";
    // Select all Orders with all the  Customers,Pizzas, Extras, Ingredients etc relatedto that Order joined to one big table
    $orders = $conn->query("SELECT ORD.ID,
                                           OI.quantity as Quantity,
                                           OrderExtras.ExtraNames AS Extras,
                                           Pizza.Ingredients AS Ingredients,
                                           Pizza.name AS Pizza_Name
                                    FROM `Order` AS ORD
                                    LEFT JOIN OrderItems OI on ORD.ID = OI.Order_ID
                                    LEFT JOIN
                                        (SELECT OrderItems_ID, GROUP_CONCAT(E.name SEPARATOR ', ' ) AS ExtraNames
                                        FROM OrderItem_has_Extra AS OIhE
                                        JOIN Extras E on OIhE.Extra_ID = E.ID
                                        GROUP BY OIhE.OrderItems_ID
                                        ORDER BY OIhE.OrderItems_ID
                                        ) AS OrderExtras
                                    ON OI.ID = OrderExtras.OrderItems_ID
                                    LEFT JOIN
                                         (SELECT P.ID, P.name, GROUP_CONCAT(E2.name SEPARATOR ', ') AS Ingredients
                                          FROM Pizzas P
                                          JOIN Pizza_has_Extra PhE on P.ID = PhE.Pizzas_ID
                                          JOIN Extras E2 on PhE.Extras_ID = E2.ID
                                          GROUP BY P.ID, P.name
                                         ) AS Pizza
                                    ON Pizza.ID = OI.Pizzas_ID;
                                    ");
    $_SESSION["queried_orders"] = $orders->fetch_all(MYSQLI_ASSOC);
    $customer = $conn->query("SELECT ORD.ID AS `Order_ID`,C.*
                                    FROM `Order` AS ORD
                                    LEFT JOIN Customer C on C.ID = ORD.Customer_ID
                                    ORDER BY Order_ID;");
    $_SESSION["queried_customer"] = $customer->fetch_all(MYSQLI_ASSOC);

    //Delete customer (and trough foreign key on-delete constraints all of its orders, order-items and associative tables
    if($_SERVER["REQUEST_METHOD"] == "POST" and isset($_POST["delete_customer_id"])) {
        $stmt = $conn->prepare("DELETE FROM Customer WHERE ID=?");
        $stmt->bind_param("i", $_POST["delete_customer_id"]);
        $stmt->execute();
    }

        //close connection
    $conn->close();

    //html output
    echo '<h1>Übersicht aller Bestellungen</h1>';
    foreach ($_SESSION["queried_customer"] as $customer){
        echo '<div class="container border mt-2 bg-light text-dark fs-4">';
            echo '<span class="fw-bold"> Bestellnummer ' . $customer["Order_ID"] . '</span>';
            echo '<hr class="mt-1">';
            echo '<div class="row">';
                    // customer info
                    echo '<div class="fs-5 col-sm-4  bg-light">';
                        echo '<p class="fs-4 fw-bold">Kunde: </p>';
                        echo  $customer["firstname"] . ', ' . $customer["lastname"];
                        if ($customer["street"] !== null and $customer["streetnumber"] !== null and $customer["zip"] !== null
                            and $customer["city"] !== null and $customer["phone"] !== null) {
                                echo '<br> ' . $customer["street"] . ', ' . $customer["streetnumber"];
                                echo '<br>' . $customer["zip"] . ', ' . $customer["city"];
                                echo '<br> Telefon:' . $customer["phone"];
                        }
                    echo '</div>';
                    // order info
                    echo '<div class="fs-5 col-sm-8 text-dark">';
                        echo '<div class="row justify-content-start">';
                        echo '<p class="fs-4 fw-bold">Bestellung: </p>';
                        foreach ($_SESSION["queried_orders"] as $order) {
                            if($order["ID"] === $customer["Order_ID"]) {
                                echo '<div class="col col-md-auto">';
                                    echo '<div class="fs-5 m-1 text-dark rounded p-2 bg-warning ">';
                                        echo '<span class="fw-bold">' . $order["Quantity"] . ' x ' . $order["Pizza_Name"] . '</span>';
                                        echo '<hr class="m-1">';
                                        echo  $order["Ingredients"]. ", " . $order["Extras"];
                                    echo '</div>';
                                echo '</div>';
                            }
                        }
                        echo '<div>';
                    echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<div class="col-12 col-md-3 ms-auto me-2 mb-2 text-end">';
                    echo '<form method="post" action="components/Delete_Customer.php">';
                    echo '<input type="hidden" name="delete_customer_id" value="'. $customer["ID"] .'">';
                    echo '<button type="submit" class="btn btn-danger w-100 fs-4"><i class="bi bi-trash"></i> Löschen</button>';
                    echo '</form>';
                echo '</div>';
        echo '</div>';
    echo '</div>';
    }
    if ($_SESSION["queried_customer"] === []){
        echo '<div class="w-100 fs-3 badge bg-danger p-2">Keine Bestellungen</div>';
    }


} //if not logged in, show login-form
else {

    if(isset($_SESSION["login_fail"]) and $_SESSION["login_fail"] === 1){
        echo '<div class="alert alert-danger alert-dismissible fade show fs-4" role="alert">
              Anmeldung fehlgeschlagen!
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>';
        $_SESSION["login_fail"] = 0;
    }

    echo ' 
         <form method="post" action="components/Login.php" id="login_form">
              <h1>Einloggen um Fortzufahren</h1>
              <label for="pass">Bitte Passwort eingeben</label>
              <input type="password" id="pass" name="pass" placeholder="*******">
              <input type="submit" name="submit_pass" value="Einloggen">
              <p>"Password : pizza"</p>
         </form>';
}

