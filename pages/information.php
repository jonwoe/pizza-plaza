<h4 class="mb-3">Persönliche Daten</h4>
<form action="components/Order.php" method="post">
  <div class="row g-3">
    <div class="col-sm-6">
      <label for="firstName" class="form-label">Vorname*</label>
      <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Max" value="" required>
      <div class="invalid-feedback">
      Bitte Vornamen angeben
      </div>
    </div>

    <div class="col-sm-6">
      <label for="lastName" class="form-label">Nachname*</label>
      <input type="text" class="form-control" id="lastName"  name="lastName" placeholder="Mustermann" value="" required>
      <div class="invalid-feedback">
      Bitte Nachnamen angeben
      </div>
    </div>

    <div class="col-9">
      <label for="address" class="form-label">Straße</label>
      <input type="text" name="street" class="form-control" id="address" placeholder="Grünbergerstraße">
    </div>

      <div class="col-3">
          <label for="streetnumber" class="form-label">Nr.</label>
          <input type="text" name="streetnumber" class="form-control" id="streetnumber" placeholder="1">
      </div>

    <div class="col-md-6">
      <label for="city" class="form-label">Stadt</label>
      <input type="text" name="city" class="form-control" id="city" placeholder="Gießen">
    </div>

    <div class="col-md-6">
      <label for="zip" class="form-label">PLZ</label>
      <input type="text" name="zip" class="form-control" id="zip" placeholder="35390">
    </div>

    <div class="col-12">
        <label for="tel" class="form-label">Telefon / Mobil</label>
        <input type="text" name="phone" class="form-control" id="tel" placeholder="0123/456789">
    </div>
  </div>

  <hr class="my-4">

  <button class="w-100 btn btn-warning btn-lg" type="submit">Bestellen</button>
</form>
