<div class="alert alert-warning alert-dismissible fade show fs-4" role="alert">
    Heute um <span class="fw-bold"> 33% reduziert Pizza <?php echo $_SESSION["discount_pizza"]["name"] . '</span> statt ' . $_SESSION["discount_pizza_old_price"] . ' € nur <span class="fw-bold">' .$_SESSION["discount_pizza"]["price"] . ' € </span>'?>
     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

<p class="lead">Herzlich Willkommen auf der Webseite von Pizza Plaza!</p>
<p>Wir sind ein fiktives Restaurant in Gießen/Langgöns. Auf unserer Webseite erfahren Sie nähere Informationen über uns, Wege, uns zu kontaktieren und erhalten bald die Möglichkeit, Bestellungen online durchzuführen.</p>
<p>Ihr Restaurant Pizza Plaza</p>