<?php
//create an info-block for each pizza. It also contains a form and a bootstrap-modal to submit the form and choosing the extras
foreach ($_SESSION["queried_pizzas"] as $pizza) {
    echo '<div class="row bg-light border text-dark px-3 py-3 m-2 fs-5">
        <div class="col-md-8">
            <!-- form which submits the selected pizza, extras and quantity -->
            <form method="post" id="pizza'.$pizza["ID"]. '" action="components/Order_Item.php">
            <input type="hidden" name="pizza_ID" value="' . $pizza["ID"] . '">
            <input type="hidden" name="pizza_name" value="' . $pizza["name"] . '">
            <input type="hidden" name="pizza_ingredients" value="' . $pizza["ingredients"] . '">
            <input type="hidden" name="pizza_description" value="' . $pizza["description"] . '">
            <p class="fw-bold"> Nr. ' . $pizza["ID"] . ' Pizza ' . $pizza["name"] . ' <br><span class="font-italic fw-normal"> ' . $pizza["description"] . ' </span></p>
            <p> Zutaten: <span class="badge bg-warning m-1 fs-6 text-wrap">' . $pizza["ingredients"] . '</span></p>
        </div>
        <div class="col col-md-4">
            <p class="text-end">' . number_format($pizza["price"], 2) . '€ pro Pizza</p>
            <p> <input class="form-control-sm" required name="amount" type="number" value="1" min="1" max="30"> </p> 
            <!-- button to trigger the modal and select the extras -->
            <button type="button" class="btn btn-warning w-100" data-bs-toggle="modal" data-bs-target="#ModalPizza'.$pizza["ID"].'"><i class="bi bi-cart-fill"></i> Zum Warenkorb hinzufügen</button>
        </div>
    </div> 
           <!-- bootstrap-modal wich pops up when you click the order button and lets you select your wished extras -->    
           <div class="modal fade" id="ModalPizza'. $pizza["ID"] .'" tabindex="-1" aria-labelledby="ModalPizza'. $pizza["ID"] .'" aria-hidden="true">
               <div class="modal-dialog modal-dialog-centered">
                   <div class="modal-content">
                       <div class="modal-header">
                           <h5 class="modal-title" id="ModalLabel">Extras auswählen</h5>
                       </div>
                       <div class="modal-body d-flex justify-content-start flex-wrap">';
                            foreach ($_SESSION["queried_extras"] as $extra) {
                                if ($extra["isChoosable"]==true){
                                echo '<div class="mt-1 ms-1">                                       
                                        <input type="checkbox" class="btn-check" name="extras[]" id="extra' . $pizza["ID"] . $extra["ID"] . '" value="' . $extra["ID"] . '" autocomplete="off">
                                        <label class="btn btn-outline-warning" for="extra' . $pizza["ID"] . $extra["ID"] . '" ' . $extra["ID"] . '">' . $extra["name"] . " +" . $extra["price"] . ' €</label>
                                      </div>';
                                }
                            }
    echo'              </div>
                       <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Zurück</button>
                            <button type="submit" form="pizza'.$pizza["ID"].'" value="Submit" class="btn btn-warning"><i class="bi bi-cart-fill"></i> Zum Warenkorb hinzufügen</button>
                       </div>
                   </div>
               </div>
           </div>
        </form>';}