<?php

include_once "Functions.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    session_start();
    //delete order item if a delete_id is passed
    if (isset($_POST["delete_id"])){
        unset($_SESSION["orders"][$_POST["delete_id"]]);
    }

    if (isset($_POST["amount"]) and isset($_POST["change_id"])){
        $item = &$_SESSION["orders"][$_POST["change_id"]];
        $item["amount"] = $_POST["amount"];
        if (isset($_POST["extras"])) {
            $item["extras"] = get_extras_with_prize($_POST["extras"]);
        }
        else{
            $item["extras"] = [];
        }
    }

    calculate_order_price($_SESSION["orders"]);

}

header("Location: ../?site=checkout");