<?php

include_once "Functions.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    session_start();

    // searching for duplicated orders and if found just change the amount, else add a new order to all orders of this session
    $order = array('pizza' => get_pizza_with_prize($_POST["pizza_ID"],$_POST["pizza_name"],$_POST["pizza_description"],$_POST["pizza_ingredients"]), 'extras' => get_extras_with_prize($_POST["extras"]), 'amount' => $_POST["amount"]);
    $item_found = false;
    foreach ($_SESSION["orders"] as &$order_item) {
        if($order_item["pizza"] === $order["pizza"] and $order_item["extras"] === $order["extras"]){
            $order_item["amount"] = ($order_item["amount"] + $order["amount"]);
            $item_found = true;
            break;
        }
    }
    if(!$item_found){array_push($_SESSION["orders"], $order);}

calculate_order_price($_SESSION["orders"]);

//print_r($_SESSION['orders']);
}

header("Location: ../?site=order");