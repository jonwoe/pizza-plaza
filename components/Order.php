<?php

if(session_status() !== PHP_SESSION_ACTIVE) session_start();

if($_SERVER["REQUEST_METHOD"] == "POST") {

    if ($_SESSION["price_order"] == 0){
        $_SESSION["order_error"] = 1;
    }

    //save everything to the DB if the first and last name are correctly sent by the information-form
    if (isset($_POST["lastName"]) and isset($_POST["firstName"]) and $_SESSION["price_order"]!=0) {

        include_once( __DIR__ . "/../config/db_config.php");

        // Create connection
        $conn = new mysqli(DBHOST, DBUSER, DBPWD, DBNAME);
        $conn->set_charset("utf8");

        // Check connection
        if ($conn->connect_error) {
            die("Es ist ein Problem aufgetreten, bitte versuchen Sie es später erneut");
        }
        echo "<script> console.log('Connected successfully') </script>";


        // save the name and address of the customer to the Customer Table
        $stmt = $conn->prepare("INSERT INTO Customer (firstname, lastname, street, streetnumber, zip, city, phone) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssiss", $firstname, $lastname, $street, $streetnumber, $zip, $city, $phone);

        $firstname = $_POST["firstName"];
        $lastname = $_POST["lastName"];

        if (isset($_POST["street"]) and $_POST["street"] !== '') {$street = $_POST["street"];} else $street = null;
        if (isset($_POST["streetnumber"]) and $_POST["streetnumber"] !== '') {$streetnumber = $_POST["streetnumber"];} else $streetnumber = null;
        if (isset($_POST["zip"]) and $_POST["zip"] !== '') {$zip = $_POST["zip"];} else $zip = null;
        if (isset($_POST["city"]) and $_POST["city"] !== '') {$city = $_POST["city"];} else $city = null;
        if (isset($_POST["phone"]) and $_POST["phone"] !== '') {$phone = $_POST["phone"];} else $phone = null;

        $stmt->execute();
        $customer_id = $stmt->insert_id;
        $stmt->close();

        //save the order to the ORDER table
        $conn->query("INSERT INTO `Order` (`Customer_ID`) VALUES (".$customer_id.")");
        $order_id = $conn->insert_id;

        foreach ($_SESSION["orders"] as $order){
            $stmt = $conn->prepare("INSERT INTO `OrderItems` (quantity, `Order_ID`, `Pizzas_ID`) VALUES (?, ?, ?)");
            $stmt->bind_param("iii", $quantity, $o_ID, $p_ID);
            // set parameters and execute
            $quantity = $order["amount"];
            $o_ID = $order_id;
            $p_ID = $order["pizza"]["ID"];
            $stmt->execute();

            $orderItem_id = $conn->insert_id;

            foreach ($order["extras"] as $extra){
                $stmt = $conn->prepare("INSERT INTO `OrderItem_has_Extra` (`OrderItems_ID`, `Extra_ID`) VALUES (?, ?)");
                $stmt->bind_param("ii",$oi_ID, $e_ID);
                // set parameters and execute
                $oi_ID = $orderItem_id;
                $e_ID = $extra["ID"];
                $stmt->execute();
            }
        }

        $conn->close();
        $_SESSION["orders"]=[];
        $_SESSION["price_order"]=0;
        $_SESSION["order_success"] = 1;

    }
}
header("Location: ../?site=main");

