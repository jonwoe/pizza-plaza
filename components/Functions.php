<?php

if(session_status() !== PHP_SESSION_ACTIVE) session_start();

//calculating the price of the whole order, expects the array of all orders/order items
function calculate_order_price (&$orders)
{
    $sum_order = 0;
    foreach ($orders as &$order_item) {
        $sum_extras = 0;
        foreach ($order_item["extras"] as $extra) {
            $sum_extras += $extra['price'];
        }
        $order_item["price_item"] = ($order_item["pizza"]["price"] + $sum_extras) * $order_item["amount"];
        $sum_order += $order_item["price_item"];
    }
    $_SESSION["price_order"] = $sum_order;
}

function searchMultiArray($val, $array) {
    foreach ($array as $element) {
        if (isset($element['ID']) and $element['ID'] === $val) {
            return true;
        }
    }
    return false;
}

// the price gets added from the db-query to the extras and pizza in the order again. It was not posted trough the form, so the price can not be manipulated trough manual post requests.
function get_extras_with_prize ($extras){
    $extras_with_price = [];
        foreach ($extras as $extra) {
            foreach ($_SESSION['queried_extras'] as $queried_extra) {
                if ($queried_extra["ID"] == $extra) {
                    array_push($extras_with_price, ["ID" => $extra, "price" => $queried_extra["price"], "name" => $queried_extra["name"]]);
                }
            }
        }
    return $extras_with_price;
}


// the price gets added from the db-query to the extras and pizza in the order again. It was not posted trough the form, so the price can not be manipulated trough manual post requests.
function get_pizza_with_prize($pizza_id,$pizza_name,$pizza_description,$pizza_ingredients){
        $pizza_with_price = [];
        foreach ($_SESSION["queried_pizzas"] as $queried_pizza) {
            if ($queried_pizza["ID"] == $pizza_id) {
                $pizza_with_price = ["ID" => $queried_pizza["ID"], "price" => $queried_pizza["price"], "name" => $pizza_name, "ingredients" => $pizza_ingredients , "description" => $pizza_description];
            }
        }
        return $pizza_with_price;
}
