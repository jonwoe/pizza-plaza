<?php

if(session_status() !== PHP_SESSION_ACTIVE) session_start();

if ($_SESSION['logged_in'] == 1 and $_SERVER["REQUEST_METHOD"] == "POST" and isset($_POST["delete_customer_id"])) {

    include_once( __DIR__ . "/../config/db_config.php");

    // Create connection
    $conn = new mysqli(DBHOST, DBUSER, DBPWD, DBNAME);
    $conn->set_charset("utf8");

    // Check connection
    if ($conn->connect_error) {
        die("Es ist ein Problem aufgetreten, bitte versuchen Sie es später erneut");
    }
    echo "<script> console.log('Connected successfully') </script>";

    //Delete customer (and trough foreign key on-delete constraints all of its orders, order-items and associative tables
    $stmt = $conn->prepare("DELETE FROM Customer WHERE ID=?");
    $stmt->bind_param("i", $_POST["delete_customer_id"]);
    $stmt->execute();

    //close connection
    $conn->close();
}
header("Location: ../?site=backend");
