SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS Customer;
DROP TABLE IF EXISTS Pizzas;
DROP TABLE IF EXISTS `Order`;
DROP TABLE IF EXISTS OrderItems;
DROP TABLE IF EXISTS Pizza_has_Extra;
DROP TABLE IF EXISTS OrderItem_has_Extra;
DROP TABLE IF EXISTS Extras;
SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE Customer (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	firstname VARCHAR(30),
	lastname VARCHAR(30),
	street VARCHAR(30),
	streetnumber VARCHAR(8),
	zip INT,
	city VARCHAR(30),
	phone VARCHAR(20)
);


CREATE TABLE Pizzas (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(20),
	price FLOAT(5,2),
	description TEXT
);

CREATE TABLE Extras (
	ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(20),
	price FLOAT(5,2),
	isChoosable BOOLEAN
);

CREATE TABLE Pizza_has_Extra (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Extras_ID INT,
	Pizzas_ID INT
);

-- Pizza_has_Extra, OrderItems and OrderItem_has_Extra are associative entitys and therefore delete on cascade by default when one of the maped entrys is deleted. 
ALTER TABLE Pizza_has_Extra ADD FOREIGN KEY (Extras_ID) REFERENCES Extras(ID) ON DELETE CASCADE;
ALTER TABLE Pizza_has_Extra ADD FOREIGN KEY (Pizzas_ID) REFERENCES Pizzas(ID) ON DELETE CASCADE;



CREATE TABLE `Order` (
	ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`timestamp` TIMESTAMP NOT NULL,
	Customer_ID INT NOT NULL
);

-- There shouldn't be orders without a customer ordering. Therefore they are also getting delted cascading.
ALTER TABLE `Order` ADD FOREIGN KEY (Customer_ID) REFERENCES Customer(ID) ON DELETE CASCADE;

CREATE TABLE OrderItems (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	quantity TINYINT,
	Order_ID INT NOT NULL,
	Pizzas_ID INT NOT NULL
);

ALTER TABLE OrderItems ADD FOREIGN KEY (Order_ID) REFERENCES `Order`(ID) ON DELETE CASCADE;
ALTER TABLE OrderItems ADD FOREIGN KEY (Pizzas_ID) REFERENCES Pizzas (ID) ON DELETE CASCADE;


CREATE TABLE OrderItem_has_Extra(
	ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Extra_ID int NOT NULL,
	OrderItems_ID int NOT NULL
);

ALTER TABLE OrderItem_has_Extra ADD FOREIGN KEY (Extra_ID) REFERENCES Extras (ID) ON DELETE CASCADE;
ALTER TABLE OrderItem_has_Extra ADD FOREIGN KEY (OrderItems_ID) REFERENCES OrderItems (ID) ON DELETE CASCADE;





-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 04. Jun 2018 um 09:06
-- Server-Version: 10.1.32-MariaDB
-- PHP-Version: 7.2.5

-- --------------------------------------------------------

--
-- Daten fÃ¼r Tabelle `Extras`
--

INSERT INTO `Extras` (`ID`, `name`, `price`, `isChoosable`) VALUES
(1, 'Tomaten', 1, 0),
(2, 'Käse', 1, 0),
(3, 'Sardellen', 1, 1),
(4, 'Oliven', 1, 1),
(5, 'Salami', 0.5, 1),
(6, 'Schinken', 0.5, 1),
(7, 'Champignons', 0.75, 1),
(8, 'Paprika', 0.5, 1),
(9, 'Mozzarella', 1, 1),
(10, 'Basilikum', 0.75, 1),
(11, 'Peperoniwurst', 1, 1),
(12, 'Zwiebeln', 0.5, 1),
(13, 'Knoblauch', 0.5, 1);


-- --------------------------------------------------------

--
-- Daten fÃ¼r Tabelle `Pizzas`
--

INSERT INTO `Pizzas` (`ID`, `name`, `price`, `description`) VALUES
(4, 'Magherita', 4.00, 'Eine leckere KÃ¤sepizza.'),
(5, 'Napoli', 5.50, 'Ein Mix aus Sardellen und Oliven bieten den ultimativen Flair.'),
(6, 'Salami', 4.70, 'Der Klassiker unter den Pizzen.'),
(7, 'Prosciutto', 4.70, 'Der im Volksmund auch als Schinkenpizza bekannte Klassiker.'),
(8, 'Funghi', 4.70, 'Die beste Wahl fÃ¼r alle Pilzliebhaber.'),
(9, 'Salami-Paprika', 5.50, 'Die beste Kombination zwischen deftig und frisch!'),
(10, 'Tricolore', 5.50, '100% vegetarisch - 100% lecker!'),
(11, 'Diavolo', 5.50, 'Teuflisch scharf!'),
(12, 'Roma', 5.50, 'Schinken und Champignons, kÃ¶stlich im Steinofen zubereitet.');

-- --------------------------------------------------------

--
-- Daten fÃ¼r Tabelle `Pizza_has_Extra`
--

INSERT INTO `Pizza_has_Extra` (`ID`, `Pizzas_ID`, `Extras_ID`) VALUES
(1, 4, 1),
(2, 4, 2),
(3, 5, 1),
(4, 5, 2),
(5, 5, 3),
(6, 5, 4),
(7, 6, 1),
(8, 6, 2),
(9, 6, 5),
(10, 7, 1),
(11, 7, 2),
(12, 7, 6),
(13, 8, 1),
(14, 8, 2),
(15, 8, 7),
(16, 9, 1),
(17, 9, 2),
(18, 9, 5),
(19, 9, 8),
(20, 10, 1),
(21, 10, 2),
(22, 10, 9),
(23, 10, 10),
(24, 11, 1),
(25, 11, 2),
(26, 11, 11),
(27, 11, 12),
(28, 11, 13),
(29, 12, 1),
(30, 12, 2),
(31, 12, 11),
(32, 12, 12),
(33, 12, 13);


INSERT INTO `Customer` VALUES (1,'Max','Mustermann','Grünbergerstraße','1',12345,'Gießen','0123456789'),(2,'Anna','Mustermann','Grünbergerstraße','1',12345,'Gießen','0123456789'),(3,'Bernd','Brot','Grünbergerstraße','11',12345,'Gießen','0123456789'),(4,'Pizza','Mann','Grünbergerstraße','1',12345,'Gießen','0123456789'),(5,'Theo','Test','Teststraße','1',35390,'Gießen','0123/456789');

INSERT INTO `Order` VALUES (1,'2021-05-14 09:23:02',1),(2,'2021-05-14 09:23:02',2),(3,'2021-05-14 09:23:02',3),(4,'2021-05-14 09:23:02',4),(5,'2021-05-14 09:23:02',5);

INSERT INTO `OrderItems` VALUES (1,1,1,4),(2,2,1,5),(3,3,1,6),(4,1,1,7),(5,2,2,8),(6,3,2,9),(7,1,2,10),(8,2,2,11),(9,3,3,12),(10,4,3,4),(11,5,4,7),(12,6,4,8),(13,1,4,8),(14,2,4,9),(15,3,5,5);

INSERT INTO `OrderItem_has_Extra` VALUES (1,1,1),(2,2,1),(3,3,2),(4,4,2),(5,5,3),(6,6,4),(7,7,5),(8,8,6),(9,9,7),(10,10,8);


